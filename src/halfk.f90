Subroutine halfk(N_sites, N_up, N_dn, Proj_k, Phi_T,one_walker,invO_up, invO_dn)
    use walkers
    implicit none
    integer, intent(in) :: N_sites, N_up, N_dn
    complex(kind=kind(0.d0)), dimension(N_sites, N_sites), intent(in):: Proj_k
    complex(kind=kind(0.d0)), dimension(N_sites, N_up+N_dn), intent(in) :: Phi_T
    Type(Walker), intent(inout) :: one_walker
    complex(kind=kind(0.d0)), dimension(N_up,N_up), intent(inout) :: invO_up
    complex(kind=kind(0.d0)), dimension(N_dn,N_dn), intent(inout) :: invO_dn
    ! local
    complex(kind=kind(0.d0)), dimension(N_sites, N_up+N_dn) :: phi_new 
    complex(kind=kind(0.d0)), dimension(N_up+N_dn, N_sites) :: phi_T_H
    complex(kind=kind(0.d0)) :: invO_up_det, invO_dn_det  
    complex(kind=kind(0.d0)) :: O, O_new, O_ratio


    
    call M_Multi_z(N_sites, N_sites, N_up+N_dn, Proj_k,one_walker%phi,phi_new) ! phi_new = exp(-\tau*H_k) * phi
    call M_Trans_z(N_sites, N_up+N_dn, phi_T, phi_T_H)  ! phi_T' = (phi_T)^{H}
    call M_Multi_z(N_up, N_sites, N_up, phi_T_H(1:N_up,:),phi_new(:,1:N_up),invO_up) ! O_up = phi_T' * phi_new 
    call M_Multi_z(N_dn, N_sites, N_dn, phi_T_H(N_up+1:N_up+N_dn,:), phi_new(:,N_up+1:N_up+N_dn),invO_dn) ! O_dn = phi_T'*phi_new 
    call s_inv_z(N_up, invO_up) ! O_up^{-1}
    call s_inv_z(N_dn, invO_dn) ! O_dn^{-1}

    call s_det_z(N_up, invO_up, invO_up_det) ! 1/det(O_up)
    call s_det_z(N_dn, invO_dn, invO_dn_det) ! 1/det(O_dn)

    O = one_Walker%O       ! O_old
    O_new = 1.d0/(invO_up_det*invO_dn_det)  ! o_new = det(O_dn) * det(O_up)
    O_ratio = O_new/O ! ratio
    one_walker%Phi = Phi_new ! new phi

    ! update
    if (real(O_ratio) > 0) then 
        one_Walker%O = O_new   
        one_Walker%W = one_Walker%W * real(O_ratio)
    else
        one_Walker%W = 0
    endif

end subroutine halfk

Subroutine halfk_bk(N_sites, N_up, N_dn, Proj_k, one_walker,invO_up, invO_dn)
    use walkers
    implicit none
    integer, intent(in) :: N_sites, N_up, N_dn
    complex(kind=kind(0.d0)), dimension(N_sites, N_sites), intent(in):: Proj_k
    Type(BackWalker), intent(inout) :: one_walker
    complex(kind=kind(0.d0)), dimension(N_up,N_up), intent(inout) :: invO_up
    complex(kind=kind(0.d0)), dimension(N_dn,N_dn), intent(inout) :: invO_dn
    ! local
    complex(kind=kind(0.d0)), dimension(N_sites, N_up+N_dn) :: phi_new 
    complex(kind=kind(0.d0)), dimension(N_sites, N_sites)   :: Proj_k_H
    complex(kind=kind(0.d0)), dimension(N_sites, N_sites)   :: Btau_up_new
    complex(kind=kind(0.d0)), dimension(N_sites, N_sites)   :: Btau_dn_new
    complex(kind=kind(0.d0)), dimension(N_up+N_dn, N_sites) :: phi_l_H
    complex(kind=kind(0.d0)) :: invO_up_det, invO_dn_det  
    complex(kind=kind(0.d0)) :: O, O_new, O_ratio

    
    call M_Trans_z(N_sites, N_sites, Proj_k, Proj_k_H)
    call M_Multi_z(N_sites, N_sites, N_up+N_dn, Proj_k_H,one_walker%Phi_l,phi_new) ! phi_new = exp(-\tau*H_k)**{H}  * phi
    call M_Trans_z(N_sites, N_up+N_dn, phi_new, Phi_l_H)
    call M_Multi_z(N_up, N_sites, N_up, phi_l_H(1:N_up,:),one_walker%Phi_r(:,1:N_up),invO_up) ! O_up = phi_T' * phi_new 
    call M_Multi_z(N_dn, N_sites, N_dn, phi_l_H(N_up+1:N_up+N_dn,:), one_walker%Phi_r(:,N_up+1:N_up+N_dn),invO_dn) ! O_dn = phi_T'*phi_new 
    call s_inv_z(N_up, invO_up) ! O_up^{-1}
    call s_inv_z(N_dn, invO_dn) ! O_dn^{-1}

    call s_det_z(N_up, invO_up, invO_up_det) ! 1/det(O_up)
    call s_det_z(N_dn, invO_dn, invO_dn_det) ! 1/det(O_dn)

    O = one_Walker%O       ! O_old
    O_new = 1.d0/(invO_up_det*invO_dn_det)  ! o_new = det(O_dn) * det(O_up)
    O_ratio = O_new/O ! ratio
    one_walker%Phi_l = Phi_new ! new phi

    ! update
    if (real(O_ratio) > 0) then 
        one_Walker%O = O_new   
        one_Walker%W = one_Walker%W * real(O_ratio)
    else
        one_Walker%W = 0
    endif

end subroutine halfk_bk




