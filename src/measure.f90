subroutine measurement(N_sites, N_up, N_dn, Phi_T, invO_up, invO_dn,one_walker,Green_up, Green_dn)

    use walkers
    implicit none

    integer,intent(in):: N_sites, N_up, N_dn
    complex(kind=Kind(0.d0)), dimension(N_sites,N_up+N_dn),intent(in) :: Phi_T
    complex(kind=Kind(0.d0)), dimension(N_up,N_up),intent(in) :: invO_up
    complex(kind=Kind(0.d0)), dimension(N_dn,N_dn),intent(in) :: invO_dn
    Type(Walker), intent(inout) :: one_walker
    complex(kind=Kind(0.d0)), dimension(N_sites, N_sites),intent(inout) :: Green_up
    complex(kind=Kind(0.d0)), dimension(N_sites, N_sites),intent(inout) :: Green_dn

    !real(kind=Kind(0.d0)),intent(out) :: energy
    

    ! local
    integer :: i, j
    complex(kind=Kind(0.d0)), dimension(N_sites,N_up) :: temp_up
    complex(kind=Kind(0.d0)), dimension(N_sites,N_dn) :: temp_dn
    complex(kind=Kind(0.d0)), dimension(N_sites,N_sites) :: Gup
    complex(kind=Kind(0.d0)), dimension(N_sites,N_sites) :: Gdn
    complex(kind=Kind(0.d0)), dimension(N_up+N_dn, N_sites) :: Phi_T_H
    complex(kind=kind(0.d0)), dimension(N_sites,N_up+N_dn) :: Phi

    Phi = one_walker%Phi
    call M_Multi_z(N_sites, N_up, N_up, Phi(:,1:N_up), &
                    invO_up, temp_up)
    call M_Multi_z(N_sites, N_dn, N_dn, Phi(:,N_up+1:N_up+N_dn), &
                    invO_dn, temp_dn)
    call M_Trans_z(N_sites, N_up+N_dn, Phi_T, Phi_T_H)
    call M_Multi_z(N_sites, N_up, N_sites, temp_up, &
            Phi_T_H(1:N_up, :), Gup)
    call M_Multi_z(N_sites, N_dn, N_sites, temp_dn, &
            Phi_T_H(N_up+1:N_up+N_dn,:), Gdn)

    Green_up = Gup + Green_up
    Green_dn = Gdn + Green_dn
end subroutine measurement

subroutine measure_obser(N_sites, ham_U, H_k, Num, Gup, Gdn, Obs)
    implicit none
    integer, intent(in) :: N_sites, Num
    real(kind=Kind(0.d0)), intent(in) :: ham_U
    complex(kind=Kind(0.d0)), dimension(N_sites, N_sites), intent(in) :: H_k
    complex(kind=Kind(0.d0)), dimension(N_sites, N_sites), intent(in) :: Gup, Gdn
    real(kind=Kind(0.d0)), dimension(Num), intent(inout) :: Obs

    integer :: i, j
    complex(kind=kind(0.d0)) :: Zkin, Zpot, ZEner

    !write(6,*) H_k(:,:)
    Zkin = cmplx(0.d0, 0.d0, kind(0.d0))
    do i = 1, N_sites
        do j = 1, N_sites
            Zkin = Zkin + H_k(i,j)*(Gup(j,i)+Gdn(j,i))
        enddo
    enddo
    Obs(1) = real(Zkin)
    
    Zpot = cmplx(0.d0, 0.d0, kind(0.d0))
    do i = 1, N_sites
        Zpot = Zpot + ham_U*Gup(i,i)*Gdn(i,i)
    enddo
    Obs(2) = real(Zpot)
    
    ZEner = cmplx(0.d0, 0.d0, kind(0.d0))
    ZEner = Zpot + Zkin
    !write(6,*) ZEner
    !write(6,*) real(ZEner)
    !write(6,*) "before", Obs(3)
    Obs(3) = real(ZEner)
    !write(6,*) "after", Obs(3)
end subroutine measure_obser

subroutine Back_measurement(N_sites, N_up, N_dn, one_walker, Green_up, Green_dn)
    use walkers
    implicit none
    integer, intent(in) :: N_sites, N_up, N_dn
    Type(backWalker), intent(inout) :: one_walker
    complex(kind=Kind(0.d0)), dimension(N_sites, N_sites), intent(inout) :: Green_up   
    complex(kind=Kind(0.d0)), dimension(N_sites, N_sites), intent(inout) :: Green_dn

    ! local
    complex(kind=Kind(0.d0)), dimension(N_up+N_dn, N_sites) :: phi_l_H
    complex(kind=Kind(0.d0)), dimension(N_sites, N_up+N_dn) :: phi_r
    complex(kind=Kind(0.d0)), dimension(N_sites, N_up+N_dn) :: phi_l
    complex(kind=Kind(0.d0)), dimension(N_up, N_up) :: invO_up
    complex(kind=Kind(0.d0)), dimension(N_dn, N_dn) :: invO_dn

    complex(kind=Kind(0.d0)), dimension(N_sites, N_up) :: temp_up
    complex(kind=Kind(0.d0)), dimension(N_sites, N_dn) :: temp_dn
    complex(kind=Kind(0.d0)), dimension(N_sites, N_sites) :: Gup
    complex(kind=Kind(0.d0)), dimension(N_sites, N_sites) :: Gdn
    phi_r = one_walker%phi_r
    phi_l = one_walker%phi_l
    call M_Trans_z(N_sites, N_up+N_dn, one_walker%phi_l, phi_l_H) 
    call M_Multi_z(N_up, N_sites, N_up, phi_l_H(1:N_up, :), phi_r(:,1:N_up), invO_up) 
    call M_Multi_z(N_dn, N_sites, N_dn, phi_l_H(N_up+1:N_up+N_dn, :), phi_r(:,N_up+1:N_up+N_dn), invO_dn) 
    call s_inv_z(N_up, invO_up)
    call s_inv_z(N_dn, invO_dn)
    
    ! get green function
    call M_Multi_z(N_sites, N_up, N_up, &
            Phi_r(:,1:N_up), invO_up, temp_up)
    call M_Multi_z(N_sites, N_dn, N_dn, &
            Phi_r(:,N_up+1:N_up+N_dn), invO_dn, temp_dn)
    call M_Multi_z(N_sites, N_up, N_sites, &
            temp_up, phi_l_H(1:N_up, :), Gup)
    call M_Multi_z(N_sites, N_dn, N_sites, &
            temp_dn, phi_l_H(N_up+1:N_up+N_dn, :), Gdn)
    
    Green_up = Gup
    Green_dn = Gdn
end subroutine Back_measurement

