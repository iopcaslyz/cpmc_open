module Obser
    implicit none

    Type Obser_real
        character(len=64) :: filename
        Integer :: N_obs
        real(kind=kind(0.d0)), Pointer :: Obs(:)
    end Type Obser_real

    Type Obser_cmplx
        character(len=64) :: filename
        Integer :: N_obs
        real(kind=kind(0.d0)), Pointer :: Obs(:)
    end Type Obser_cmplx
contains

Pure subroutine R_obser_make(one_Obser,Num,filename)
    implicit none
    Type(Obser_real), intent(inout) :: one_Obser
    Integer, Intent(in) :: Num
    character(len=64), Intent(in) :: filename

    one_Obser%filename = filename
    one_Obser%N_obs = Num
    Allocate(one_Obser%Obs(Num))

end subroutine R_obser_make

Pure subroutine C_obser_make(one_Obser,Num,filename)
    implicit none
    Type(Obser_cmplx), intent(inout) :: one_Obser
    Integer, Intent(in) :: Num
    character(len=64), Intent(in) :: filename

    one_Obser%filename = filename
    one_Obser%N_obs = Num
    Allocate(one_Obser%Obs(Num))

end subroutine C_obser_make

Pure subroutine Init_Robser(one_Obser)
    implicit none
    Type(obser_real), intent(inout) :: one_Obser
    one_Obser%Obs(:) = 0.d0 
end subroutine Init_Robser


Pure subroutine Init_Cobser(one_Obser)
    implicit none
    Type(obser_cmplx), intent(inout) :: one_Obser
    one_Obser%Obs(:) = cmplx(0.d0, 0.d0, kind(0.d0))
end subroutine Init_Cobser

Subroutine Print_obs_scal(one_Obser)
    implicit none
    Type(Obser_real), intent(in) :: one_Obser

    character(len=64) :: filename, File_suff

    File_suff = "_scal"
    filename = trim(adjustl(one_Obser%filename))//File_suff
    open(unit=1999,file=filename,status='unknown',action="write",position="append")
    write(1999,*) one_obser%Obs(1)
    close(1999)

end subroutine Print_obs_scal

end module Obser
