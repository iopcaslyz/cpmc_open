Module Hamiltonian
    use walkers
    use Obser
    
    Implicit none

    ! lattice parameter
    Integer,                 public :: Lx, Ly
    Integer,                 public :: N_dn, N_up
    Real (Kind=Kind(0.d0)),  public :: ham_Tx, ham_Ty
    Real (Kind=Kind(0.d0)),  public :: ham_U
    Real (Kind=Kind(0.d0)),  public :: k_x, k_y
    Real (Kind=Kind(0.d0)),  public :: dtau
    Real (kind=Kind(0.d0)),  public :: Tau_bk
    Integer,                 public :: N_bk


    Real (Kind=Kind(0.d0)),  public :: frac_norm
    Integer,                 public :: N_wlk
    Integer,                 public :: N_blkstep   ! the number of step per block
    Integer,                 public :: N_eqblk ! number of equilibration block
    Integer,                 public :: N_blk
    Integer,                 public :: itv_modsvd
    Integer,                 public :: itv_pc
    Integer,                 public :: itv_Em

    Integer,                 public :: N_sites
    Integer,                 public :: N_par


    Complex (Kind=Kind(0.d0)), allocatable, public :: H_k(:,:), U_k(:,:)
    Complex (Kind=Kind(0.d0)), allocatable, public :: Proj_k(:,:) !exp(-k/2 * H_k)
    Complex (Kind=Kind(0.d0)), allocatable, public :: Phi_T(:,:)
    Complex (kind=Kind(0.d0)), public :: aux_fld(2,2)
    Real (Kind=Kind(0.d0)), allocatable, public :: E_k(:)
    Real (Kind=Kind(0.d0)),  public :: ek, ev, et

    Complex (Kind=Kind(0.d0)), allocatable, public :: invO_up(:,:), invO_dn(:,:)

    ! walkers
    Type(Walker), allocatable, public :: RandomWalkers(:)
    ! observe
    Type(Obser_real), allocatable, public :: Obser_scal(:)
    real(kind=Kind(0.d0)), allocatable :: Obs(:)
    logical :: flag

    ! weight
    real(kind=Kind(0.d0)) :: W_tmp
    real(kind=Kind(0.d0)) :: Wall



contains

    subroutine Init_Spring
        use spring
        implicit none

        integer :: system_time, stream_seed
#ifdef MPI
        include 'mpif.h'
        Integer :: Isize, Irank
        Integer :: STATUS(MPI_STATUS_SIZE)
        Integer :: ierr
#else
        Integer :: Irank
#endif



#ifdef MPI
        CALL MPI_COMM_SIZE(MPI_COMM_WORLD,ISIZE,IERR)
        CALL MPI_COMM_RANK(MPI_COMM_WORLD,IRANK,IERR)
#else
        Irank = 0
#endif

        call system_clock(system_time)
        stream_seed = abs( system_time - (1981 + Irank + 2008) * 213)
        call spring_sfmt_init(stream_seed)
    end subroutine Init_Spring
    


    subroutine  Init_Lattice


        implicit none

#ifdef MPI
        include 'mpif.h'
#endif
        
        logical   ::   istat
        integer   ::   i
        
        namelist /model_para/ Lx, Ly, N_dn, N_up, ham_Tx, ham_Ty, ham_U, k_x, k_y, dtau, Tau_bk
        namelist /ctrl_para/ N_wlk, N_blkstep, N_eqblk, N_blk, itv_modsvd, itv_pc, itv_Em

#ifdef MPI
        Integer :: ierr
        Integer :: Isize, Irank
        Integer :: STATUS(MPI_STATUS_SIZE)
        CALL MPI_COMM_SIZE(MPI_COMM_WORLD,ISIZE,IERR)
        CALL MPI_COMM_RANK(MPI_COMM_WORLD,IRANK,IERR)
#endif

#ifdef MPI
        If(Irank == 0) then
#endif
            istat = .false.
            inquire(file='readin.txt',exist=istat)
            if ( istat .eqv. .true.) then
                open(unit=1634, file='readin.txt',status='unknown')
                read(1634,model_para)
                read(1634,ctrl_para)
            endif

#ifdef MPI
        ENDIF
        CALL MPI_BCAST(Lx,           1, MPI_INTEGER, 0, MPI_COMM_WORLD,ierr)
        CALL MPI_BCAST(Ly,           1, MPI_INTEGER, 0, MPI_COMM_WORLD,ierr)
        CALL MPI_BCAST(N_dn,         1, MPI_INTEGER, 0, MPI_COMM_WORLD,ierr)
        CALL MPI_BCAST(N_up,         1, MPI_INTEGER, 0, MPI_COMM_WORLD,ierr)
        CALL MPI_BCAST(ham_Tx,       1, MPI_REAL8,   0, MPI_COMM_WORLD,ierr)
        CALL MPI_BCAST(ham_Ty,       1, MPI_REAL8,   0, MPI_COMM_WORLD,ierr)
        CALL MPI_BCAST(k_x,          1, MPI_REAL8,   0, MPI_COMM_WORLD,ierr)
        CALL MPI_BCAST(k_y,          1, MPI_REAL8,   0, MPI_COMM_WORLD,ierr)
        CALL MPI_BCAST(dtau,         1, MPI_REAL8,   0, MPI_COMM_WORLD,ierr)
        CALL MPI_BCAST(Tau_bk,       1, MPI_REAL8,   0, MPI_COMM_WORLD,ierr)

        CALL MPI_BCAST(N_wlk,        1, MPI_INTEGER, 0, MPI_COMM_WORLD,ierr)
        CALL MPI_BCAST(N_blkstep,    1, MPI_INTEGER, 0, MPI_COMM_WORLD,ierr)
        CALL MPI_BCAST(N_eqblk,      1, MPI_INTEGER, 0, MPI_COMM_WORLD,ierr)
        CALL MPI_BCAST(N_blk,        1, MPI_INTEGER, 0, MPI_COMM_WORLD,ierr)
        CALL MPI_BCAST(itv_modsvd,   1, MPI_INTEGER, 0, MPI_COMM_WORLD,ierr)
        CALL MPI_BCAST(itv_pc,       1, MPI_INTEGER, 0, MPI_COMM_WORLD,ierr)
        CALL MPI_BCAST(itv_Em,       1, MPI_INTEGER, 0, MPI_COMM_WORLD,ierr)
#endif

        N_sites = Lx*Ly
        N_par = N_up+N_dn
        N_bk = int(Tau_bk/dtau)
        !write(6,*) "Tau_bk", Tau_bk
        !write(6,*) "dtau", dtau
        !write(6,*) "N_bk=", N_bk

#ifdef MPI
        If (Irank == 0) then
#endif
            Open(UNIT = 50, FILE="info",STATUS="unknown",position="append")
            write(50,*) "============================="
            write(50,*) "Lx          :",         Lx
            write(50,*) "Ly          :",         Ly
            write(50,*) "N_sites     :",         N_sites
            write(50,*) "N_dn        :",         N_dn
            write(50,*) "N_up        :",         N_up
            write(50,*) "N_par       :",         N_par
            write(50,*) "ham_Tx      :",         ham_Tx
            write(50,*) "ham_Ty      :",         ham_Ty
            write(50,*) "ham_U       :",         ham_U
            write(50,*) "k_x         :",         k_x
            write(50,*) "k_y         :",         k_y
            write(50,*) "dtau        :",         dtau
            write(50,*) "Tau_bk      :",         Tau_bk
            write(50,*) "N_bk        :",         N_bk
            write(50,*) "N_wlk       :",         N_wlk
            write(50,*) "N_blkstep   :",         N_blkstep
            write(50,*) "N_eqblk     :",         N_eqblk
            write(50,*) "N_blk       :",         N_blk
            write(50,*) "itv_modsvd  :",         itv_modsvd
            write(50,*) "itv_pc      :",         itv_pc
            write(50,*) "itv_Em      :",         itv_Em
            close(50)
#ifdef MPI
        endif
#endif


        allocate(H_K(N_sites,N_sites))
        call Get_H_k_square(N_sites,Lx,Ly,k_x,k_y,ham_Tx,ham_Ty,H_k)
    
    end subroutine Init_Lattice


    subroutine Init_Ham_K

        implicit none
        !local parameter
        integer i,  j
        integer n0, n1

        !! Get H_k matrix
        allocate(U_k(N_sites, N_sites))
        allocate(Proj_k(N_sites, N_sites))
        allocate(E_k(N_sites))
        allocate(Phi_T(N_sites, N_par))
           
        call Get_Proj(0.5*dtau, N_sites, H_k, E_k, U_k, Proj_k)
        call Get_PhiT(N_sites, N_up,N_dn,U_k,Phi_T)
        call Get_Total_energy(ham_U, N_sites, N_up, N_dn, Phi_T, E_k, ek, ev, et)
        
    end subroutine Init_Ham_K

    subroutine Init_Walkers

        implicit none
        integer :: i

        allocate(RandomWalkers(N_wlk))
        allocate(invO_up(N_up, N_up))
        allocate(invO_dn(N_dn, N_dn))
        do i = 1, N_wlk
            call Walker_make(RandomWalkers(i), N_sites, N_up, N_dn, Phi_T)
        enddo
    end subroutine Init_Walkers


    subroutine Init_Ham_V

        implicit none
        
        !local 
        integer :: i, j
        real(kind=Kind(0.d0)) :: gamma

        frac_norm = (et-0.5*ham_U*N_par)*dtau
        gamma=acosh(exp(0.5*dtau*ham_U))
        do i = 1,2
            do j = 1,2
                aux_fld(i,j)=exp(gamma*(-1)**(i+j))
            enddo
        enddo
    end subroutine Init_Ham_V

    subroutine Back_Propagate(Num, BackWalkers, obs_wlk)
        implicit none

        integer, intent(in) :: Num
        Type (BackWalker), dimension(N_wlk),intent(inout) :: BackWalkers
        real(kind=Kind(0.d0)), dimension(N_wlk, Num) :: obs_wlk
        ! local
        complex(kind=Kind(0.d0)), dimension(N_sites, N_up+N_dn) :: Phi_tmp
        complex(kind=Kind(0.d0)), allocatable :: Gup_walkers(:,:,:)
        complex(kind=Kind(0.d0)), allocatable :: Gdn_walkers(:,:,:)
        real(kind=Kind(0.d0)) :: W_tmp
        complex(kind=Kind(0.d0)) :: O_tmp
        integer :: i_wlk, j_site, i_bk

        allocate(Gup_walkers(N_wlk, N_sites, N_sites))
        allocate(Gdn_walkers(N_wlk, N_sites, N_sites))
        Gup_walkers = cmplx(0.d0, 0.d0, kind(0.d0))
        Gdn_walkers = cmplx(0.d0, 0.d0, kind(0.d0))
        do i_wlk = 1, N_wlk
            Phi_tmp = RandomWalkers(i_wlk)%Phi
            W_tmp = RandomWalkers(i_wlk)%w
            O_tmp = RandomWalkers(i_wlk)%O
            call BackWalker_make(BackWalkers(i_wlk), N_sites, &
                                N_up, N_dn, Phi_tmp, Phi_T, W_tmp, O_tmp)
        enddo
        
        do i_bk = 1, N_bk
	        do i_wlk = 1, N_wlk
                if(BackWalkers(i_wlk)%W > 0) then
                    BackWalkers(i_wlk)%W = BackWalkers(i_wlk)%W * exp(frac_norm)
                endif
    		    call halfk_bk(N_sites, N_up, N_dn, Proj_k, BackWalkers(i_wlk),invO_up, invO_dn)  
		        if (BackWalkers(i_wlk)%W > 0) then
		            do j_site = 1, N_sites
				        if (BackWalkers(i_wlk)%W > 0) then
				            call backPov(j_site, N_up, N_dn, BackWalkers(i_wlk), invO_up, invO_dn, aux_fld)
				        endif
		            enddo
			    endif
			    if (BackWalkers(i_wlk)%W > 0) then
			        call halfk_bk(N_sites, N_up, N_dn, Proj_k, BackWalkers(i_wlk),invO_up, invO_dn)  
			    endif
                if (i_bk == N_bk) then
	                call back_measurement( N_sites, N_up, N_dn, &
                        BackWalkers(i_wlk), Gup_walkers(i_wlk, :, :), Gdn_walkers(i_wlk, :, :) )   
	                call measure_obser(N_sites, ham_U, H_k, &
                        size(obser_scal,1), Gup_walkers(i_wlk,:,:), Gdn_walkers(i_wlk,:,:), obs_wlk(i_wlk,:))  
                endif
		    enddo
		    if (mod(i_bk,itv_pc) == 0) then
		        call back_pop_contr(N_up, N_dn, N_wlk, N_sites, BackWalkers)
		    endif	
            if (mod(i_bk,itv_modsvd) == 0) then
                CALL Backstblz(N_wlk, N_sites, N_up, N_dn, BackWalkers)
            endif
	    enddo
    end subroutine Back_Propagate

    subroutine Propagate_walker(flag_mea, W0)

        implicit none
        logical, intent(in) :: flag_mea
        real(kind=Kind(0.d0)),intent(inout) :: W0

        ! local
        integer :: Num
        integer :: i_wlk, j_site
        real(kind=Kind(0.d0)), allocatable :: obs_wlk(:,:)
        Type(BackWalker), allocatable :: BackWalkers(:)

        allocate(BackWalkers(N_wlk))
        Num = Size(Obser_scal, 1)
        allocate(obs_wlk(N_wlk, Num))
        obs_wlk(:,:) = 0.d0
        do i_wlk = 1, N_wlk
            if(RandomWalkers(i_wlk)%W > 0) then
                RandomWalkers(i_wlk)%W = RandomWalkers(i_wlk)%W * exp(frac_norm)
            endif
            call halfk(N_sites, N_up, N_dn, Proj_k, Phi_T, RandomWalkers(i_wlk),invO_up, invO_dn)
            if(RandomWalkers(i_wlk)%W >0 )then
                do j_site = 1, N_sites
                    if(RandomWalkers(i_wlk)%W > 0) then
                        call Pov(j_site, N_up, N_dn, phi_T(j_site,:), RandomWalkers(i_wlk),invO_up,invO_dn, aux_fld)
                    endif
                enddo
            endif

            if(RandomWalkers(i_wlk)%W > 0 ) then
                call halfk(N_sites,N_up,N_dn,Proj_k,Phi_T,RandomWalkers(i_wlk),invO_up,invO_dn)
            endif
        enddo

        if (flag_mea) then
           call Back_Propagate(Num, BackWalkers, obs_wlk)
        endif
        if (flag_mea) then
            do i_wlk = 1, N_wlk
                if(BackWalkers(i_wlk)%W > 0)then
                    obs(:) = obs_wlk(i_wlk,:)*BackWalkers(i_wlk)%W + obs(:)                   
                    W0 = BackWalkers(i_wlk)%W + W0
                endif
            enddo
        endif
        deallocate(obs_wlk)
    end subroutine Propagate_walker

    subroutine Alloc_obser_scal
        implicit none
        !local
        integer :: i, N
        character(len=64) :: Filename
        allocate(Obser_scal(3))
        do i = 1,Size(Obser_scal,1)
            select case (i)
            case (1)
                N=1;  Filename = "kin"
            case (2)
                N=1;  Filename = "Pot"
            case (3)
                N=1;  Filename = "Ener"                
            case default
                Write(6,*) 'Error in Alloc_obser_scal'
            end select
            Call R_obser_make(Obser_scal(i),N,Filename)
        enddo

        allocate( Obs(Size(Obser_scal, 1)) )
        
    end subroutine Alloc_obser_scal

    subroutine Init_obser_scal
        implicit none
        integer :: i
        do i = 1, Size(Obser_scal, 1)
            call Init_Robser(Obser_scal(i))
        enddo
        Wall = 0.d0
        Obs(:) = 0.d0
    end subroutine Init_obser_scal

    subroutine Observe
        implicit none
#ifdef MPI
        include 'mpif.h'
        REAL(kind=Kind(0.d0)), allocatable :: tmp(:)
        REAL(kind=Kind(0.d0)) :: Wall_tmp
        Integer :: No
        Integer :: IERR
        Integer :: Isize, Irank
        Integer :: STATUS(MPI_STATUS_SIZE)

        CALL MPI_COMM_SIZE(MPI_COMM_WORLD,ISIZE,IERR)
        CALL MPI_COMM_RANK(MPI_COMM_WORLD,IRANK,IERR)
        No = size(Obser_scal,1)
        Allocate( tmp(No) )
        call mpi_reduce(obs,tmp,No,mpi_real8,mpi_sum,0,mpi_comm_world,ierr)
        call mpi_reduce(wall,Wall_tmp,1,mpi_real8,mpi_sum,0,mpi_comm_world,ierr)
        if (irank==0) then
            Wall = Wall_tmp
            obs(:) = tmp(:)
#endif
            Obs(:) = Obs(:)/Wall
            Obser_scal(1)%Obs(1) = obs(1)
            Obser_scal(2)%Obs(1) = obs(2)
            Obser_scal(3)%Obs(1) = obs(3)
#ifdef MPI
        endif
#endif
 
    end subroutine Observe

    subroutine Pr_obs
        implicit none
        integer :: i

        do i = 1, size(Obser_scal,1)
            call Print_obs_scal(Obser_scal(i))
        enddo
        
    end subroutine Pr_obs
end module Hamiltonian
