subroutine make_square_Lattice(Lx, Ly, List_xy, nnlist)
implicit none
integer,intent(in) :: Lx, Ly
integer,dimension(Lx,Ly), intent(inout) ::  List_xy
integer,dimension(Lx*Ly,4), intent(inout) ::  nnlist

!!local parameter 

integer :: i, j, nc
integer :: n0, n1, n2, n3, n4

nc=0
do j = 1, Ly
    do i = 1, Lx
        nc=nc+1
        List_xy(i,j) = nc
    enddo
enddo 

do j = 1, Ly
    do i = 1, Lx
        n0 = List_xy(i,j) 

        if (i==Lx) then
            n1 = List_xy(1,j)
        else
            n1 = List_xy(i+1,j)
        endif

        if (i==1) then
            n3 = List_xy(Lx,j)
        else
            n3 = List_xy(i-1,j)
        endif

        if (j==Ly) then
            n2 = List_xy(i,1)
        else
            n2 = List_xy(i,j+1)
        endif

        if (j==1) then
            n4 = List_xy(i,Ly)
        else
            n4 = List_xy(i,j-1)
        endif
        nnlist(n0,1) = n1
        nnlist(n0,2) = n2
        nnlist(n0,3) = n3
        nnlist(n0,4) = n4
        
    enddo
enddo

end subroutine make_square_Lattice

