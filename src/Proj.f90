subroutine Get_H_K_square(N_sites, Lx, Ly, k_x, k_y, ham_Tx, ham_Ty, H_k)
    implicit none
    integer, intent(in) :: N_sites, Lx, Ly
    real(kind=Kind(0.d0)), intent(in) :: k_x, k_y
    real(kind=Kind(0.d0)), intent(in) :: ham_Tx, ham_Ty
    complex(kind=Kind(0.d0)), dimension(N_sites, N_sites), intent(inout) :: H_k

    !local
    real(kind=Kind(0.d0)), parameter :: pi=acos(-1.d0)
    complex(kind=Kind(0.d0)) :: kx, ky
    integer :: ix, iy, r, i

    kx = cmplx(0.d0, pi*k_x, kind(0.d0))
    ky = cmplx(0.d0, pi*k_y, kind(0.d0))

    H_k(:,:) = cmplx(0.d0, 0.d0, kind(0.d0))
    r=0
    do iy = 1, Ly
        do ix = 1, Lx
            r=r+1
            if (Lx.ne.1) then
                if (ix.eq.1) then
                    H_k(r,r+Lx-1) = H_k(r,r+Lx-1) - ham_Tx*exp(kx)
                    H_k(r,r+1) = H_k(r,r+1) - ham_Tx
                else if (ix.eq.Lx) then
                    H_k(r,r-1) = H_k(r,r-1) - ham_Tx
                    H_k(r,r+1-Lx) = H_k(r,r+1-Lx) - ham_Tx*exp(-kx)
                else
                    H_k(r,r-1) = - ham_Tx
                    H_k(r,r+1) = - ham_Tx
                endif
            endif   

            if (Ly.ne.1) then
                if (iy.eq.1) then
                    H_k(r, r+(Ly-1)*Lx) = H_k(r, r+(Ly-1)*Lx) - ham_Ty*exp(ky)
                    H_k(r, r+Lx) = H_k(r,r+Lx) - ham_Ty
                else if (iy.eq.Ly) then
                    H_k(r, r-Lx) = H_k(r, r-Lx) - ham_Ty
                    H_k(r, r-(Ly-1)*Lx) = H_k(r, r-(Ly-1)*Lx) - ham_Ty*exp(-ky) 
                else
                    H_k(r, r-Lx) = -ham_Ty
                    H_k(r, r+Lx) = -ham_Ty
                endif
            endif  
            
        enddo
    enddo


end subroutine Get_H_K_square




subroutine Get_Proj(g,  N_sites, H_k, E_k, U_k, Proj_k)
implicit none
real(Kind=Kind(0.d0)),intent(in) :: g
integer :: N_sites
complex(Kind=Kind(0.d0)), dimension(N_sites, N_sites), intent(inout) :: H_k, U_k
real(Kind=Kind(0.d0)), dimension(N_sites), intent(inout) :: E_k
complex(Kind=Kind(0.d0)), dimension(N_sites, N_sites), intent(inout) :: Proj_k

! local parametera
integer :: n, t
real(kind=Kind(0.d0)) :: Z
complex(kind=Kind(0.d0)) :: Z1

call s_eig_he(N_sites, N_sites, H_k, E_k, U_k)

Proj_k = cmplx(0.d0, 0.d0, kind(0.d0))
do n = 1, N_sites
    Z = exp(-g*E_k(n))
    do t = 1, N_sites
        Z1 = Z*conjg(U_k(t,n)) 
        Proj_k(1:N_sites,t) = Proj_k(1:N_sites,t) + Z1*U_k(1:N_sites,n)       
    enddo
enddo

end subroutine Get_Proj


subroutine Get_PhiT(N_sites, N_up, N_dn, U_k, Phi_T)
    implicit none
    integer, intent(in) :: N_sites, N_up, N_dn
    complex(kind=Kind(0.d0)), dimension(N_sites, N_sites), intent(inout) :: U_k
    complex(kind=Kind(0.d0)), dimension(N_sites, N_up+N_dn), intent(inout) :: Phi_T


    !local parameter
    integer :: i, N_par

    N_par = N_up+N_dn
    do i = 1, N_par
        if (i > N_up) then
            Phi_T(1:N_sites, i) = U_k(1:N_sites, i-N_up)
        else
            Phi_T(1:N_sites, i) = U_k(1:N_sites, i)
        endif
    enddo

end subroutine Get_PhiT

subroutine Get_Total_energy(U,N_sites,N_up,N_dn,Phi_T,E_k,ek,ev,et)
    implicit none
    real(kind=Kind(0.d0)), intent(in) :: U
    integer, intent(in) :: N_sites, N_up, N_dn
    complex(kind=Kind(0.d0)), dimension(N_sites, N_up+N_dn),intent(inout) :: Phi_T
    real(kind=Kind(0.d0)), dimension(N_sites), intent(inout) :: E_k
    real(kind=Kind(0.d0)), intent(inout) :: ek, ev, et

    !local parameter
    complex(kind=Kind(0.d0)), dimension(N_sites,N_sites) :: Gnnup, Gnndn
    complex(kind=Kind(0.d0)), dimension(N_sites) :: N_r_up, N_r_dn
    integer :: i


    ek = sum(E_k(1:N_up))+sum(E_k(1:N_dn))
    call MProductSelf_z(N_sites,N_up,Phi_T(:,1:N_up),Gnnup)
    call MProductSelf_z(N_sites,N_dn,Phi_T(:,N_up+1:N_up+N_dn),Gnndn)
    call MDiagVec_z(N_sites,Gnnup,N_r_up)
    call MDiagVec_z(N_sites,Gnndn,N_r_dn)
    ev = 0.d0
    do i = 1, N_sites
        ev = ev + U*real(N_r_up(i)*conjg(N_r_dn(i)))
    end do
    et = ev + ek
end subroutine Get_Total_energy

