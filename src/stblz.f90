subroutine stblz(N_wlk, N_sites, N_up, N_dn, RandomWalkers)
    use walkers
    implicit none
    integer, intent(in) :: N_wlk, N_sites, N_up, N_dn
    Type(Walker), dimension(N_wlk), intent(inout) :: RandomWalkers

    complex(kind=Kind(0.d0)), dimension(N_sites, N_up+N_dn) :: Phi, Phi_q
    complex(kind=Kind(0.d0)), dimension(N_up,N_up) :: R_up
    complex(kind=Kind(0.d0)), dimension(N_dn,N_dn) :: R_dn
    complex(kind=Kind(0.d0)) :: up_det, dn_det
    integer :: i_wlk, j
    
    do i_wlk = 1, N_wlk
        up_det = cmplx(1.d0, 0.d0, kind(0.d0))
        dn_det = cmplx(1.d0, 0.d0, kind(0.d0))
        Phi(:,:) = RandomWalkers(i_wlk)%Phi(:,:)
        call M_de_qr(N_sites, N_up, Phi(:,1:N_up), Phi_q(:,1:N_up), R_up)
        call M_de_qr(N_sites, N_dn, Phi(:,N_up+1:N_up+N_dn), Phi_q(:,N_up+1:N_up+N_dn), R_dn)
        call s_det_z(N_up, R_up, up_det)
        call s_det_z(N_dn, R_dn, dn_det)
        RandomWalkers(i_wlk)%Phi(:,:) = Phi_q(:,:)
        RandomWalkers(i_wlk)%O = RandomWalkers(i_wlk)%O / up_det / dn_det
    enddo 

end subroutine stblz

subroutine Backstblz(N_wlk, N_sites, N_up, N_dn, BackWalkers)
    use walkers
    implicit none
    integer, intent(in) :: N_wlk, N_sites, N_up, N_dn
    Type(BackWalker), dimension(N_wlk), intent(inout) :: BackWalkers

    complex(kind=Kind(0.d0)), dimension(N_sites, N_up+N_dn) :: Phi, Phi_q
    complex(kind=Kind(0.d0)), dimension(N_sites, N_sites) :: Btau_up_tmp
    complex(kind=Kind(0.d0)), dimension(N_sites, N_sites) :: Btau_dn_tmp
    complex(kind=Kind(0.d0)), dimension(N_up,N_up) :: R_up
    complex(kind=Kind(0.d0)), dimension(N_dn,N_dn) :: R_dn
    complex(kind=Kind(0.d0)), dimension(N_sites,N_sites) :: R1, R2
    complex(kind=Kind(0.d0)) :: up_det, dn_det
    integer :: i_wlk, j
    
    do i_wlk = 1, N_wlk
        up_det = cmplx(1.d0, 0.d0, kind(0.d0))
        dn_det = cmplx(1.d0, 0.d0, kind(0.d0))
        Phi(:,:) = BackWalkers(i_wlk)%Phi_l(:,:)
        call M_de_qr(N_sites, N_up, Phi(:,1:N_up), Phi_q(:,1:N_up), R_up)
        call M_de_qr(N_sites, N_dn, Phi(:,N_up+1:N_up+N_dn), Phi_q(:,N_up+1:N_up+N_dn), R_dn)
        call s_det_z(N_up, R_up, up_det)
        call s_det_z(N_dn, R_dn, dn_det)
        BackWalkers(i_wlk)%Phi_l(:,:) = Phi_q(:,:)
        BackWalkers(i_wlk)%O = BackWalkers(i_wlk)%O / up_det / dn_det
    enddo 

end subroutine Backstblz
