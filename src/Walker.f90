module Walkers
    implicit none

    Type Walker
        Integer :: N_sites, N_up, N_dn
        Complex(kind=Kind(0.d0)), Pointer :: Phi(:,:)
        real(kind=Kind(0.d0)) :: W
        complex(kind=Kind(0.d0)) :: O
    end Type Walker

    Type BackWalker
        Integer :: N_sites, N_up, N_dn
        Complex(kind=Kind(0.d0)), Pointer :: Phi_r(:,:)
        Complex(kind=Kind(0.d0)), Pointer :: Phi_l(:,:)
        Complex(kind=Kind(0.d0)) :: O
        real(kind=Kind(0.d0)) :: W
    end Type BackWalker

contains

Subroutine Walker_make(one_Walker, N_sites, N_up, N_dn, Phi_T)
    implicit none
    Type (Walker), intent(inout) :: one_Walker
    Integer, Intent(in) :: N_sites, N_up, N_dn
    Complex(kind=Kind(0.d0)), dimension(N_sites,N_up+N_dn), intent(in) :: Phi_T

    ! local
    integer :: i

    Allocate(one_Walker%Phi(N_sites, N_up+N_dn))
    one_Walker%N_sites = N_sites
    one_Walker%N_up = N_up
    one_Walker%N_dn = N_dn
    one_Walker%W = 1.d0
    one_Walker%O = cmplx(1.d0, 0.d0, kind(0.d0))
    one_Walker%Phi = Phi_T
end subroutine Walker_make

Subroutine BackWalker_make(one_Walker, N_sites, N_up, N_dn, Phi_r, Phi_l, W, O)
    implicit none
    Type (BackWalker), intent(inout) :: one_Walker
    Integer, Intent(in) :: N_sites, N_up, N_dn
    Complex(kind=Kind(0.d0)), dimension(N_sites, N_up+N_dn), intent(in) :: Phi_r
    Complex(kind=Kind(0.d0)), dimension(N_sites, N_up+N_dn), intent(in) :: Phi_l
    real(kind=Kind(0.d0)),intent(in) :: W
    complex(kind=Kind(0.d0)), intent(in) :: O

    Allocate(one_Walker%Phi_r(N_sites, N_up+N_dn))
    Allocate(one_Walker%Phi_l(N_sites, N_up+N_dn))
    one_Walker%Phi_r = Phi_r
    one_Walker%Phi_l = Phi_l
    one_Walker%W = W
    one_Walker%O = O
end subroutine BackWalker_make

end module Walkers







