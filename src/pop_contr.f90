subroutine pop_contr(N_up, N_dn, N_wlk, N_sites, Randomwalkers)
    use Walkers
    use spring 

    implicit none
    integer, intent(in) :: N_up, N_dn, N_wlk, N_sites
    Type(Walker), dimension(N_wlk) :: Randomwalkers

    !local

    Type(Walker), dimension(N_wlk) :: new_Randomwalkers
    integer :: i, j, n, i_wlk
    real(kind=kind(0.d0)) :: d, sum_w, rand

    do i = 1, N_wlk
        call Walker_make(new_Randomwalkers(i), N_sites, N_up, N_dn, Randomwalkers(i)%Phi)
    enddo

    sum_w = 0.d0
    do i = 1, N_wlk
        sum_w = sum_w + Randomwalkers(i)%W
    enddo
    d = real(N_wlk)/sum_w
    !write(6,*) "d=", d
    
    i_wlk=0
    sum_w = -spring_sfmt_string()
    !write(6,*) sum_w
    do i = 1, N_wlk
        sum_w = sum_w + Randomwalkers(i)%W * d
        n = ceiling(sum_w)
        do j = i_wlk+1, n
            new_Randomwalkers(j)%Phi = Randomwalkers(i)%Phi
            new_Randomwalkers(j)%O = Randomwalkers(i)%O
        enddo 
        i_wlk = n 
    enddo

    Randomwalkers = new_Randomwalkers

end subroutine pop_contr

subroutine back_pop_contr(N_up, N_dn, N_wlk, N_sites, Backwalkers)
    use Walkers
    use spring 

    implicit none
    integer, intent(in) :: N_up, N_dn, N_wlk, N_sites
    Type(BackWalker), dimension(N_wlk) :: BackWalkers

    !local

    Type(BackWalker), dimension(N_wlk) :: new_BackWalkers
    integer :: i, j, n, i_wlk
    real(kind=kind(0.d0)) :: d, sum_w, rand

    do i = 1, N_wlk
        call BackWalker_make(new_BackWalkers(i), N_sites, &
            N_up, N_dn,BackWalkers(i)%phi_r, BackWalkers(i)%phi_l, 1.d0, BackWalkers(i)%O)
    enddo

    sum_w = 0.d0
    do i = 1, N_wlk
        sum_w = sum_w + BackWalkers(i)%W
    enddo
    d = real(N_wlk)/sum_w
    
    i_wlk=0
    sum_w = -spring_sfmt_string()
    do i = 1, N_wlk
        sum_w = sum_w + BackWalkers(i)%W * d
        n = ceiling(sum_w)
        do j = i_wlk+1, n
            new_BackWalkers(j)%phi_r = BackWalkers(i)%phi_r
            new_BackWalkers(j)%phi_l = BackWalkers(i)%phi_l
            new_BackWalkers(j)%O = BackWalkers(i)%O
        enddo 
        i_wlk = n 
    enddo
    
    BackWalkers = new_BackWalkers

end subroutine back_pop_contr
