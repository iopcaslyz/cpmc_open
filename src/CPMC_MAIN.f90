Program main

!------------------------------------------------------------------
!> @author
!> CPMC-project
!
!> @brief
!> Main program
!------------------------------------------------------------------
  use Hamiltonian
  use spring
  implicit none
#ifdef  MPI
  include 'mpif.h'
#endif

  ! local
  integer :: i_blk, j_step
  real(kind=Kind(0.d0)) :: time1, time2
#ifdef MPI
  Integer :: IERR
  Integer :: Isize, Irank
  Integer :: STATUS(MPI_STATUS_SIZE)
  CALL MPI_INIT(ierr)
  CALL MPI_COMM_SIZE(MPI_COMM_WORLD,ISIZE,IERR)
  CALL MPI_COMM_RANK(MPI_COMM_WORLD,IRANK,IERR)
#endif


#ifdef MPI
  IF (Irank==0) then
#endif
    write(6,*) "This is free software of Constrained Path Monte Carlo"
    write(6,*) "author: Yuzhi Liu"
    write(6,*) "The code is working >>>>>>>>>>"
#ifdef MPI
  ENDIF
#endif

#ifdef MPI
  IF (Irank==0) then
#endif
    call CPU_Time(time1)
#ifdef MPI
  ENDIF   
#endif
  call Init_Spring
  
!!! Initialization 
  CALL Init_Lattice
  CALL Init_Ham_K
  CALL Init_Ham_V
  CALL Init_Walkers

  CAll Alloc_obser_scal
!!! Equilibration phase
#ifdef MPI
  If (Irank==0) then
#endif
    write(6,*) "Equilibration phase >>>>>>>>"
#ifdef MPI
  ENDIF
#endif
  flag=.false.
  do i_blk = 1, N_eqblk
    do j_step = 1, N_blkstep
       CALL Propagate_walker(flag, W_tmp)
       if (mod(j_step, itv_modsvd) == 0) then
            CALL stblz(N_wlk,N_sites,N_up,N_dn,RandomWalkers)
       endif        
       if (mod(j_step, itv_pc) == 0 ) then
            CALL pop_contr(N_up, N_dn, N_wlk, N_sites, RandomWalkers)
       endif
    enddo
  enddo
!!! Measurement phase

#ifdef MPI
  If (Irank==0) then
#endif
    write(6,*) "Measurement phase >>>>>>>>"
#ifdef MPI
  ENDIF
#endif
  do i_blk = 1, N_blk

    call Init_Obser_scal
    do j_step = 1, N_blkstep
      if (mod(j_step,itv_Em)==0) then
        flag=.true.
      else
        flag=.false.
      endif
      call Propagate_walker(flag, Wall)
      if (mod(j_step,itv_modsvd) == 0) then
        CALL stblz(N_wlk, N_sites, N_up, N_dn, RandomWalkers)
      endif
      if (mod(j_step,itv_pc) == 0) then
        CAll pop_contr(N_up, N_dn, N_wlk, N_sites, RandomWalkers)
      endif
      if(mod(j_step,itv_Em) == 0) then
        frac_norm = (Obs(3)/Wall - 0.5*ham_U*N_par)*dtau
      endif
      !stop
    enddo
    !stop
    call Observe
#ifdef MPI
    if (irank==0) then
#endif
        call Pr_obs
#ifdef MPI
    endif
#endif
  enddo

#ifdef MPI
  IF (Irank==0) then
#endif
    call CPU_Time(time2)
    Open(Unit=50, FILE="info",STATUS="unknown",position="append")
        write(50,*) "time=", time2-time1, "s"
    close(50)
#ifdef MPI
  ENDIF
#endif
!!! Results

stop

end
