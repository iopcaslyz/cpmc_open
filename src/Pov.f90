subroutine Pov(i_site, N_up, N_dn, phi_T, one_walker, invO_up, invO_dn, aux_fld)
    use spring
    use Walkers
    implicit none
    integer, intent(in) :: i_site, N_up, N_dn
    complex(kind=Kind(0.d0)), dimension(N_up+N_dn), intent(inout) :: phi_T
    Type(walker), intent(inout) :: one_Walker
    complex(kind=Kind(0.d0)), dimension(N_up, N_up), intent(inout) :: invO_up
    complex(kind=Kind(0.d0)), dimension(N_dn, N_dn), intent(inout) :: invO_dn
    complex(kind=Kind(0.d0)), dimension(2,2), intent(in) :: aux_fld
    
    
    ! local
    complex(kind=Kind(0.d0)), dimension(N_up) :: temp1_up, temp2_up
    complex(kind=Kind(0.d0)), dimension(N_dn) :: temp1_dn, temp2_dn
    complex(kind=Kind(0.d0)), dimension(N_up, N_up) :: invO_up_temp
    complex(kind=Kind(0.d0)), dimension(N_dn, N_dn) :: invO_dn_temp
    complex(kind=Kind(0.d0)), dimension(N_up+N_dn) :: phi
    complex(kind=Kind(0.d0)), dimension(N_up+N_dn) :: phi_T_H
    complex(kind=Kind(0.d0)), dimension(2,2) :: Gii, temp, temp1
    complex(kind=Kind(0.d0)), dimension(2,2) :: matone, RR
    complex(kind=kind(0.d0)), dimension(2) :: O_ratio_temp
    complex(kind=Kind(0.d0)) :: temp_scal_up, temp_scal_dn
    real(kind=Kind(0.d0)), dimension(2) :: O_ratio_temp_real
    real(kind=Kind(0.d0)) :: sum_O_ratio_temp_real
    integer :: x_spin


    call M_ones(2,matone)

    phi = one_walker%Phi(i_site,:)
    call M_Trans_v_z(N_up+N_dn, phi_T, phi_T_H)
    call M_Multi_vm_z(N_up, N_up, phi(1:N_up), invO_up, temp1_up)
    call M_Multi_vm_z(N_dn, N_dn, phi(N_up+1:N_up+N_dn), invO_dn, temp1_dn)
    call M_Multi_mv_z(N_up, N_up, invO_up, phi_T_H(1:N_up), temp2_up)
    call M_Multi_mv_z(N_dn, N_dn, invO_dn, phi_T_H(N_up+1:N_up+N_dn), temp2_dn)
    call M_dot_product(N_up, temp1_up, Phi_T_H(1:N_up), Gii(1,1))
    Gii(1,2) = Gii(1,1)
    call M_dot_product(N_dn, temp1_dn, Phi_T_H(N_up+1:N_up+N_dn), Gii(2,1))
    Gii(2,2) = Gii(2,1)
    temp = aux_fld - matone
    call M_dot_Matrix(2, temp, Gii, temp1)
    RR = temp1 + matone

    O_ratio_temp(1) = RR(1,1) * RR(2,1) ! P(x=1) = P_up(x=1) * P_dn(x=1)
    O_ratio_temp(2) = RR(1,2) * RR(2,2) ! P(x=-1) = P_up(x=-1) * P_dn(x=-1)

    if( real(O_ratio_temp(1)) > 0 ) then
        O_ratio_temp_real(1) = real(O_ratio_temp(1))
    else
        O_ratio_temp_real(1) = 0.d0
    endif

    if( real(O_ratio_temp(2)) > 0 ) then
        O_ratio_temp_real(2) = real(O_ratio_temp(2))
    else
        O_ratio_temp_real(2) = 0.d0
    endif

    sum_O_ratio_temp_real = O_ratio_temp_real(1) + O_ratio_temp_real(2)

    if (sum_O_ratio_temp_real<=0)then
        one_walker%W = 0
    endif
    if (one_walker%w>0) then
        one_walker%w = one_walker%w * 0.5 * sum_O_ratio_temp_real
        if (O_ratio_temp_real(1)/sum_O_ratio_temp_real >= spring_sfmt_string()) then
            x_spin=1
        else
            x_spin=2
        endif

        one_walker%Phi(i_site,1:N_up) = phi(1:N_up) * aux_fld(1,x_spin)
        one_walker%Phi(i_site,N_up+1:N_up+N_dn) = phi(N_up+1:N_up+N_dn) * aux_fld(2,x_spin)

        one_walker%O = one_walker%O * O_ratio_temp(x_spin)
        
        temp_scal_up = (1-aux_fld(1,x_spin))/RR(1,x_spin)
        temp_scal_dn = (1-aux_fld(2,x_spin))/RR(2,x_spin)

        call M_Multi_vv_z(N_up, temp2_up, temp1_up, invO_up_temp)
        call M_Multi_vv_z(N_dn, temp2_dn, temp1_dn, invO_dn_temp)
        invO_up = invO_up + temp_scal_up * invO_up_temp
        invO_dn = invO_dn + temp_scal_dn * invO_dn_temp
    endif
end subroutine Pov


subroutine backPov(i_site, N_up, N_dn, one_walker, invO_up, invO_dn, aux_fld)
    use spring
    use Walkers
    implicit none
    integer, intent(in) :: i_site, N_up, N_dn
    Type(Backwalker), intent(inout) :: one_Walker
    complex(kind=Kind(0.d0)), dimension(N_up, N_up), intent(inout) :: invO_up
    complex(kind=Kind(0.d0)), dimension(N_dn, N_dn), intent(inout) :: invO_dn
    complex(kind=Kind(0.d0)), dimension(2,2), intent(in) :: aux_fld
    
    
    ! local
    complex(kind=Kind(0.d0)), dimension(N_up+N_dn) :: phi_r, phi_l
    complex(kind=Kind(0.d0)), dimension(N_up) :: temp1_up, temp2_up
    complex(kind=Kind(0.d0)), dimension(N_dn) :: temp1_dn, temp2_dn
    complex(kind=Kind(0.d0)), dimension(N_up, N_up) :: invO_up_temp
    complex(kind=Kind(0.d0)), dimension(N_dn, N_dn) :: invO_dn_temp
    complex(kind=Kind(0.d0)), dimension(N_up+N_dn) :: phi_l_H
    complex(kind=Kind(0.d0)), dimension(2,2) :: Gii, temp, temp1
    complex(kind=Kind(0.d0)), dimension(2,2) :: matone, RR
    complex(kind=kind(0.d0)), dimension(2) :: O_ratio_temp
    complex(kind=Kind(0.d0)) :: temp_scal_up, temp_scal_dn
    real(kind=Kind(0.d0)), dimension(2) :: O_ratio_temp_real
    real(kind=Kind(0.d0)) :: sum_O_ratio_temp_real
    integer :: x_spin


    call M_ones(2,matone)

    phi_r = one_walker%phi_r(i_site,:)
    phi_l = one_walker%phi_l(i_site,:)
    call M_Trans_v_z(N_up+N_dn, phi_l, phi_l_H)
    call M_Multi_vm_z(N_up, N_up, phi_r(1:N_up), invO_up, temp1_up)
    call M_Multi_vm_z(N_dn, N_dn, phi_r(N_up+1:N_up+N_dn), invO_dn, temp1_dn)
    call M_Multi_mv_z(N_up, N_up, invO_up, phi_l_H(1:N_up), temp2_up)
    call M_Multi_mv_z(N_dn, N_dn, invO_dn, phi_l_H(N_up+1:N_up+N_dn), temp2_dn)
    call M_dot_product(N_up, temp1_up, Phi_l_H(1:N_up), Gii(1,1))
    Gii(1,2) = Gii(1,1)
    call M_dot_product(N_dn, temp1_dn, Phi_l_H(N_up+1:N_up+N_dn), Gii(2,1))
    Gii(2,2) = Gii(2,1)
    temp = aux_fld - matone
    call M_dot_Matrix(2, temp, Gii, temp1)
    RR = temp1 + matone

    O_ratio_temp(1) = RR(1,1) * RR(2,1) ! P(x=1) = P_up(x=1) * P_dn(x=1)
    O_ratio_temp(2) = RR(1,2) * RR(2,2) ! P(x=-1) = P_up(x=-1) * P_dn(x=-1)

    if( real(O_ratio_temp(1)) > 0 ) then
        O_ratio_temp_real(1) = real(O_ratio_temp(1))
    else
        O_ratio_temp_real(1) = 0.d0
    endif

    if( real(O_ratio_temp(2)) > 0 ) then
        O_ratio_temp_real(2) = real(O_ratio_temp(2))
    else
        O_ratio_temp_real(2) = 0.d0
    endif

    sum_O_ratio_temp_real = O_ratio_temp_real(1) + O_ratio_temp_real(2)

    if (sum_O_ratio_temp_real<=0)then
        one_walker%W = 0
    endif
    if (one_walker%w>0) then
        one_walker%w = one_walker%w * 0.5 * sum_O_ratio_temp_real
        if (O_ratio_temp_real(1)/sum_O_ratio_temp_real >= spring_sfmt_string()) then
            x_spin=1
        else
            x_spin=2
        endif


        !Btau
        !one_walker%Btau_up(i_site,:) = one_walker%Btau_up(i_site,:) * aux_fld(1,x_spin)
        !one_walker%Btau_dn(i_site,:) = one_walker%Btau_dn(i_site,:) * aux_fld(2,x_spin)

        one_walker%Phi_l(i_site,1:N_up) = phi_l(1:N_up) * conjg(aux_fld(1,x_spin))
        one_walker%Phi_l(i_site,N_up+1:N_up+N_dn) = phi_l(N_up+1:N_up+N_dn) * conjg(aux_fld(2,x_spin))

        one_walker%O = one_walker%O * O_ratio_temp(x_spin)
        
        temp_scal_up = (1-aux_fld(1,x_spin))/RR(1,x_spin)
        temp_scal_dn = (1-aux_fld(2,x_spin))/RR(2,x_spin)

        call M_Multi_vv_z(N_up, temp2_up, temp1_up, invO_up_temp)
        call M_Multi_vv_z(N_dn, temp2_dn, temp1_dn, invO_dn_temp)
        invO_up = invO_up + temp_scal_up * invO_up_temp
        invO_dn = invO_dn + temp_scal_dn * invO_dn_temp
    endif
end subroutine backPov
