#!/bin/bash

NP=12
ND=1

source cal_para.sh

WORKDIR="$PWD"
echo $WORKDIR
EXE=$HOME/weilunJiang/xydqmc-master/src/exe
cd $WORKDIR
for L in ${Larray}; do
  for U_b in ${U_barray}; do
    for beta in ${beta_array}; do
      cd $WORKDIR

      maindir=L${L}Beta${beta}superfluid
      if [ ! -d $maindir ]; then
          mkdir $maindir
      fi
      cd $maindir

      #jobdir=t${t_b}
      jobdir=U${U_b}_md_dt${dt}
      if [ ! -d $jobdir ]; then
          mkdir $jobdir
      fi
      cd $jobdir

      if [ -f ../U${U_b}/confout ]; then
          cp ../U${U_b}/confout confin
      else
          cp $WORKDIR/confin .
      fi

      #if [ -f confout ]; then
      #    cp confout confin
      #else
      #    cp $WORKDIR/confin .
      #fi

#t_b          : $(echo "scale=6;1/$t_b/$dtau"|bc)

cat>ftdqmc.in<<endin
L            : $L
dtau         : $dtau
beta         : $beta
mu           : $mu 
dope         : $dope
llocal       : $llocal
nsw_stglobal : $nsw_stglobal
nsweep       : $nsweep
nwarnup      : $nwarnup
nbin         : $nbin
xmag         : $xmag
flux_x       : $flux_x
flux_y       : $flux_y
ltau         : $ltau
nwrap        : $nwrap
nuse         : $nuse
rt           : $rt
t_b          : $t_b
U_b          : $U_b
K_bf         : $K_bf
dx           : $dx
order        : $order
gcount       : $gcount
mdstep       : $mdstep
dt           : $dt
pm           : $pm
lfourier     : $lfourier
endin

cat>L${L}U${U_b}.sub<<endsub
#!/bin/bash
yhrun -n $NP -N $ND -p TH_NEW  $EXE
endsub
     rm -f slurm*
     yhbatch  -n $NP -N $ND -p TH_NEW  L${L}U${U_b}.sub
    done
  done
done
