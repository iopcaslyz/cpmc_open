#!/bin/bash

source cal_para.sh

WORKDIR="$PWD"
echo $WORKDIR
EXE=../../../src/exe
cd $WORKDIR
for Lx  in ${Lxarray}; do
    for ham_U in ${Uarray}; do
        cd $WORKDIR

        maindir=Lx${Lx}
        if [ ! -d $maindir ]; then
            mkdir $maindir
        fi
        cd $maindir

        jobdir=ham_U${ham_U}
        if [ ! -d $jobdir ]; then
            mkdir $jobdir
        fi
        cd $jobdir

        if [ -f confout ]; then
            cp confout confin
        fi
		#cp $WORKDIR/Heff.para .

cat>readin.txt<<endin
&model_para
Lx=$Lx
Ly=$Ly
N_dn=$N_dn
N_up=$N_up
ham_Tx=$ham_Tx
ham_Ty=$ham_Ty
ham_U=$ham_U
k_x=$k_x
k_y=$k_y
dtau=$dtau
Tau_bk=$Tau_bk
/
&ctrl_para
N_wlk=$N_wlk
N_blkstep=$N_blkstep
N_eqblk=$N_eqblk
N_blk=$N_blk
itv_modsvd=$itv_modsvd
itv_pc=$itv_pc
itv_Em=$itv_Ew
/
endin
        echo "Running job for Lx${Lx}/ham_U${ham_U} ..."
        $EXE
    done
done
