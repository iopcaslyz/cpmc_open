#!/bin/bash

# model_para
Lxarray=$(echo '3')
Ly=4
N_up=3
N_dn=3
ham_Tx=1
ham_Ty=1
k_x=0.02
k_y=0.04
Uarray=$(awk 'BEGIN{for(i=4;i<=4.001;i+=0.50) printf("%6.2f",i)}')
dtau=0.01
Tau_bk=2.0

#ctrl_para

N_wlk=100
N_blkstep=40
N_eqblk=100
N_blk=20
itv_modsvd=5
itv_pc=40
itv_Ew=40
